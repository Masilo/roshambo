package za.co.entelect.masilo.bokang;
import domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Created by abednigo.masilo on 2017/06/06.
 */
public class Roshambo {
    static Logger logger = LoggerFactory.getLogger(Roshambo.class);
    public static void main( String[] args ) throws IOException{
        logger.info("Running Main");
        Game run = new Game();
        run.gameLoop();
    }
}
