package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.entelect.masilo.bokang.Roshambo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by abednigo.masilo on 2017/06/09.
 */
public class BattleRules {
    static Logger logger = LoggerFactory.getLogger(BattleRules.class);
    public BattleRules(){
        configRules();
    }
    private int _size;
    private Vector<String> _options;
    private Vector<String> _rulesVector;

    private void configRules(){
        logger.info("Retrieving configurations from file");
        File file = new File("config.txt");
        Vector<String> rulesVector = new Vector<String>();

        try{
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                String rule = scanner.nextLine();
                StringTokenizer stringTokenizer = new StringTokenizer(rule);
                _size = stringTokenizer.countTokens();

                for(int i = 0; i < _size; i++){
                    rulesVector.add(stringTokenizer.nextToken());
                }
            }

            _options = new Vector<String>();
            _options.add(rulesVector.elementAt(0));
            _options.add(rulesVector.elementAt(1));
            _options.add(rulesVector.elementAt(3));
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
    public Vector<String> getRules(){
        return _options;
    }
}
