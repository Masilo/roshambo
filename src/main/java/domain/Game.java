package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.entelect.masilo.bokang.Roshambo;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by abednigo.masilo on 2017/06/08.
 */
public class Game {
    static Logger logger = LoggerFactory.getLogger(Game.class);
    public Game(){
        // Initialize Game State
        _terminate = false;
    }

    private int _userChoice;
    private int _computerChoice;
    private boolean _terminate;

    public void gameLoop(){
        // Run loop while not terminated
        logger.info("Running GameLoop");
        while(!_terminate){
            displayGameIntro();
            getGameChoices();
            if(!_terminate){
                displayBattleResults();
            }
        }
    }

    // Private methods
    private void displayGameIntro(){
        // Introduce game and instructions etc
        System.out.println("\n");
        BattleRules battleRules = new BattleRules();
        System.out.println("Select one of the options below:");
        System.out.println("0: Exit");
        int option = 1;
        for (int i = 0; i < battleRules.getRules().size(); i++){
            System.out.println(option+": "+battleRules.getRules().elementAt(i));
            option++;
        }
    }
    private void getGameChoices(){
        logger.info("Getting Game Choices");
        // Get user input
        Scanner scanner = new Scanner(System.in);
        _userChoice = scanner.nextInt();
        if(_userChoice == 0) _terminate = true;

        // Get Computer input
        Random random = new Random();
        _computerChoice = random.nextInt(3) + 1;
    }
    private void displayBattleResults() {
        logger.info("Getting Battle Results");
        // initiate Battle object
        Battle battle = new Battle(_userChoice, _computerChoice);
        BattleRules battleRules = new BattleRules();
        System.out.println("User: " + battleRules.getRules().elementAt(_userChoice-1) + " vs Computer: " + battleRules.getRules().elementAt(_computerChoice-1));

        // Get Game results
        GameResults results;
        results = battle.getBattleResults();
        switch (results) {
            case Win: System.out.println("You Win!");
                break;
            case Draw: System.out.println("It's a Draw!");
                break;
            case Lose: System.out.println("You Lose!");
                break;
            default: System.out.println("Try Again");
                break;
        }
    }
}
