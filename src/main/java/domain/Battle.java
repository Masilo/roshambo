package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.entelect.masilo.bokang.Roshambo;

/**
 * Created by abednigo.masilo on 2017/06/07.
 * Battle is the comparison class between the different properties (Rock, Paper, Scissor)
 * It compares the abilities of each against the other
 */
public class Battle {
    static Logger logger = LoggerFactory.getLogger(Battle.class);
    public Battle(int player1,int player2){
        _player1 = player1;
        _player2 = player2;
    }

    private int _player1;
    private int _player2;
    private GameResults _gameResults;

    public GameResults getBattleResults(){
        logger.info("Evaluating battle results");
        if(_player1 == _player2){
            return GameResults.Draw;
        }
        if((_player2 == 2 && _player1 == 1)|| (_player2 == 3 && _player1 == 2)||(_player2 == 1 && _player1 == 3)){
            _gameResults = GameResults.Win;
        }
        if((_player2 == 2 && _player1 == 3)|| (_player2 == 3 && _player1 == 1)||(_player2 == 1 && _player1 == 2)){
            _gameResults = GameResults.Lose;
        }
        return _gameResults;
    }
}
