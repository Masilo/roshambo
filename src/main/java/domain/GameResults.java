package domain;

/**
 * Created by abednigo.masilo on 2017/06/08.
 */
public enum GameResults {
        Draw,
        Win,
        Lose
}
